{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "a7f4e963",
   "metadata": {},
   "source": [
    "Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.\n",
    "SPDX-License-Identifier: Apache-2.0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20cbc162",
   "metadata": {},
   "source": [
    "# Visualization - Grouping and Appearance Customization\n",
    "\n",
    "In this notebook we'll examine how to use the `--group-by` feature of the `%%gremlin` magic to group vertices and how to customize the appearance of these groups.  \n",
    "\n",
    "**Note** This notebook builds on the visualization patterns explained in [Air-Routes-Gremlin](Air-Routes-Gremlin.ipynb) notebook, so if you are not familiar with how the visualzation feaure works we recommend reviewing that prior to beginning this notebook.\n",
    "\n",
    "This notebook uses the air-routes dataset to demonstrate some of the different options available.  If your cluster does not already contain this data then run the cell below to load the data into your Neptune cluster otherwise you can skip the next cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3232a5e5",
   "metadata": {},
   "outputs": [],
   "source": [
    "%seed --language Gremlin --dataset airports --run"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df4617ec",
   "metadata": {},
   "source": [
    "With the air-routes data now loaded we are ready to begin looking at how to group and customize our result visualization.  \n",
    "\n",
    "## Grouping Options\n",
    "\n",
    "When visualizing the results of a query the grouping of the vertices follows a couple of rules:\n",
    "\n",
    "* If no grouping property is specified and the label for a vertex is returned then the vertices are grouped by label.\n",
    "    * If the label does not exist in the results then all vertices are grouped into a single group\n",
    "* To group vertices by a specific property use the `--group-by` or `-g` switch on the `%%gremlin` line magic.  \n",
    "    * The name provided must be a case sensitive match to the property name of the vertex (i.e. `Name` will not group by the `name` property)\n",
    "    * If the name specified does not match any property of a vertex then that vertex is grouped in a default group\n",
    "* If you would like to not group by any property then the `--ignore-groups` flag will not group the vertices\n",
    "\n",
    "### Default Grouping\n",
    "\n",
    "Let's take a look at what our air-routes graph looks like if we run a query and use the default grouping. Try running the query below and clicking on the Graph tab to see all the vertices connected to Cozumel."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0d2eac8a",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%gremlin\n",
    "\n",
    "g.V().has('airport', 'code','CZM').both().path()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f70c0847",
   "metadata": {},
   "source": [
    "From the results we see three groups each represented by a different color.  Each of the vertices is added to a group based on it's label.  \n",
    "\n",
    "Grouping also occurs when we use the `by()` modulators to specify additional return fields.  Running the query below to see how the items are grouped when we find all connected vertices for Cozumel and return all their properties and tokens (T.id and T.label)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0b7074b1",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%gremlin\n",
    "\n",
    "g.V().has('code', 'ANC').both().path().by(valueMap().with(WithOptions.tokens))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c461a12b",
   "metadata": {},
   "source": [
    "From the resultant visualization we see that we still have three groups of vertices, grouped by the label.  \n",
    "\n",
    "However, what would happen if we don't return the label in the results.  Run the query below to see how the items are grouped when we find all connected vertices for Cozumel and return just their properties, not the label."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b697ec16",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%gremlin\n",
    "\n",
    "g.V().has('code', 'ANC').both().path().by(valueMap())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f992ea07",
   "metadata": {},
   "source": [
    "From the results we see that all our vertices are in a single group now and colored the same.  When our results do not contain the label for the vertex we must then specify the property we want to group by.  \n",
    "\n",
    "### Specifying the property to group by\n",
    "\n",
    "To specify the property to group by we use either the `--group-by` or `-g` switch followed by the property name.  The property name needs to be a case sensitive match for the name in the vertex.  \n",
    "\n",
    "**Note** Finding the property name can be accomplished using the Details View and clicking on a vertex.  This includes the `T.id` and `T.label` properties.\n",
    "\n",
    "Let's run the query below and see what our grouping looks like if we find all routes from Cozumel and group them by their `country`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c4e0331f",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%gremlin -p v,inv -g country\n",
    "g.V().hasLabel('airport').has('code','CZM').out('route').path().by(valueMap())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d51905df",
   "metadata": {},
   "source": [
    "As we see our results are now split into three groups (MX/US/CA) based on the country property.  \n",
    "\n",
    "Let's run the query below and see what our grouping looks like if we find all connections to Cozumel and group them by their `country`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d1b51d2a",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%gremlin -p v,inv -g country\n",
    "g.V().hasLabel('airport').has('code','CZM').both().path().by(valueMap())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "776edf46",
   "metadata": {},
   "source": [
    "The resultant graph looks very similar to the previous one except that their is now four groups instead of three.  Unlike in the previous example where we only returned `route` connections, this query returned all connected vertices so our resultset came back with a `continent` and `country` vertex in addition to the `airport` vertices.  Neither the `country` nor `continent` vertices have a `country` property.  When a vertex does not contain the property specified to group by then that vertex is put into a default group.  This same behavior occurs when you use the incorrect casing for the property name, as shown in the query below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d337fae1",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%gremlin -p v,inv -g Country\n",
    "g.V().hasLabel('airport').has('code','CZM').out('route').path().by(valueMap())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8f75778",
   "metadata": {},
   "source": [
    "### Grouping on different properties for each label"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3bf09b32",
   "metadata": {},
   "source": [
    "While the `continent` and `country` vertices cannot be grouped on properties exclusive to `airport` vertices, we do have the ability to add additional properties with which to form new groups. To do this, we will make use of Jupyter's built-in line magic variable injection to pass in a variable containing a JSON-format string, where we can specify what property we want to use to group each of the individual vertex labels.  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e500b5c8",
   "metadata": {},
   "source": [
    "The group-by variable must be defined in following format:  \n",
    "\n",
    "`groups_var = '{\"label_1\":{\"groupby\":\"property_1\"},\"label_2\":{\"groupby\":\"property_2\"}}'`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "158189c0",
   "metadata": {},
   "source": [
    "Let's try defining group-by with individual properties for `airport`, `continent`, and `country`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aa2dab8e",
   "metadata": {},
   "outputs": [],
   "source": [
    "groups_var = '{\"airport\":{\"groupby\":\"country\"},\"country\":{\"groupby\":\"desc\"},\"continent\":{\"groupby\":\"code\"}}'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "327ccddd",
   "metadata": {},
   "source": [
    "Then, we can rerun our previous query, this time with `$groups_var`, and see that the `continent` and `country` vertices now belong to their own groups based on the specified properties.  \n",
    "\n",
    "**Note** we also need to use `valueMap(true)` so that the label names and relevant properties can be accessed for group matching."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2f0fb09",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%gremlin -p v,inv -g $groups_var\n",
    "g.V().hasLabel('airport').has('code','CZM').both().path().by(valueMap(true))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bade0fd5",
   "metadata": {},
   "source": [
    "### Ignoring Grouping\n",
    "\n",
    "In certain situations you may want your visualization to contain no groups.  This can be accomplished by adding the `--ignore-groups` flag to your query as shown by running the next cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3c34d478",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%gremlin -p v,inv --ignore-groups\n",
    "g.V().hasLabel('airport').has('code','CZM').out('route').path().by(valueMap())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a4eadb7",
   "metadata": {},
   "source": [
    "Now that we know how to group our vertices together let's take a look at how to customize the appearance of these groups.\n",
    "\n",
    "## Group Customization\n",
    "The Amazon Neptune Notebooks use an open source library called [Vis.js](https://github.com/visjs) to assist with drawing the graph diagrams. Vis.js provides a rich set of customizable settings. The documentation for most of the visualization settings used in this notebook can be found [here](https://visjs.org/) and in particular the graph network drawing documentation can be found [here](https://visjs.github.io/vis-network/docs/network/).  \n",
    "\n",
    "To see the current settings used by your notebook you can use the `%graph_notebook_vis_options` line magic command. Try running the cell below.  \n",
    "\n",
    "To change any of these settings create a new cell and use `%%graph_notebook_vis_options` to change them (note the two percent signs indicating a cell magic).\n",
    "\n",
    "To customize the appearance of the the groups we want to use the [groups](https://visjs.github.io/vis-network/docs/network/groups.html#) options.  Their is a nearly endless amount of customization you can make to the groups using the options provided but we will demonstrate some of the most common ones in the next few sections.\n",
    "\n",
    "### Specifying Colors\n",
    "\n",
    "Specifying the colors of groups is probably one of the most common customizations performed.  To accomplish this we specify the options using the `%%graph_notebook_vis_options` magic as shown below.  For each of the associated group names we use the exact property value followed by the options you would like to use for that group.\n",
    "\n",
    "**Note** Finding the exact property value for the group name can be accomplished by looking at the data returned in the Console tab.\n",
    "\n",
    "Run the next two cells to set the colors for our three groups to red for the airports in Canada, green for the airports in the US, and blue for the airports in Mexico.  In the case of color, the values can be specified by name, RGBA value, or Hex value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aece55f6",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%graph_notebook_vis_options\n",
    "{\n",
    "  \"groups\": {\n",
    "      \"['CA']\": {\"color\": \"red\"},\n",
    "      \"['MX']\": {\"color\": \"rgba(9, 104, 178, 1)\"},      \n",
    "      \"['US']\": {\"color\": \"#00FF00\"}\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "516ea768",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%gremlin -p v,inv -g country\n",
    "g.V().hasLabel('airport').has('code','CZM').out('route').path().by(valueMap())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a178a54f",
   "metadata": {},
   "source": [
    "### Specifying Shape\n",
    "\n",
    "In addition to specifying the color of the groups you are also able to customize the shape of the vertex using one of the following options. \n",
    "\n",
    "The types with the label inside of it are: `ellipse`, `circle`, `database`, `box`, `text`.\n",
    "\n",
    "The ones with the label outside of it are: `diamond`, `dot`, `star`, `triangle`, `triangleDown`, `hexagon`, and `square`\n",
    "\n",
    "Run the cells below to see what our visualization looks like with shapes specified."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f02b919b",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%graph_notebook_vis_options\n",
    "{\n",
    "  \"groups\": {\n",
    "      \"['CA']\": {\"color\": \"red\", \"shape\": \"box\"},\n",
    "      \"['MX']\": {\"color\": \"rgba(9, 104, 178, 1)\" , \"shape\": \"oval\"},      \n",
    "      \"['US']\": {\"color\": \"#00FF00\", \"shape\": \"star\"}\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f7f8eed2",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%gremlin -p v,inv -g country\n",
    "g.V().hasLabel('airport').has('code','CZM').out('route').path().by(valueMap())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d969158e",
   "metadata": {},
   "source": [
    "### Specifying Icons and Images\n",
    "\n",
    "In addition to specifying shapes icons and images can also be specified to represent our groups.  \n",
    "\n",
    "Icons can either be from `Ionicons` or `FontAwesome` version 4 or 5.  Icons are specified by setting the `shape` to `icon` and then specifyign the `code` for the icon.\n",
    "\n",
    "Images must be either local to the notebook or publically available on the internet.  Images are specified by setting the `shape` to `image` or `circularImage` and then setting the `image` property to the address of the image to display.\n",
    "\n",
    "Running the two cells below will display the Mexico group using an image of the Mexican flag and the US group using an airplane icon from FontAwesome."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "450de168",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%graph_notebook_vis_options\n",
    "{\n",
    "  \"groups\": {\n",
    "      \"['CA']\": {\"color\": \"red\"},\n",
    "    \"['MX']\": {\"shape\": \"image\", \n",
    "               \"image\":\"https://cdn.countryflags.com/thumbs/mexico/flag-round-250.png\"},\n",
    "    \n",
    "    \"['US']\": {\n",
    "      \"shape\": \"icon\",\n",
    "      \"icon\": {\n",
    "        \"face\": \"FontAwesome\",\n",
    "        \"code\": \"\\uf072\",\n",
    "        \"color\": \"#00FF00\"\n",
    "          }\n",
    "      }\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "40cd2de9",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%gremlin -p v,inv -g country\n",
    "g.V().hasLabel('airport').has('code','CZM').out('route').path().by(valueMap())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2f676ac7",
   "metadata": {},
   "source": [
    "### Specifying Size\n",
    "\n",
    "The last customization we are going to demonstrate is the ability to set the size for the group.  This is accomplished by setting the `size` property of the group. \n",
    "\n",
    "**Note** Only shapes that do not have the label inside them are impacted by this property."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d0981894",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%graph_notebook_vis_options\n",
    "{\n",
    "  \"groups\": {\n",
    "      \"['CA']\": {\"color\": \"red\", \"size\": 3},\n",
    "    \"['MX']\": {\"shape\": \"image\", \n",
    "               \"image\":\"https://cdn.countryflags.com/thumbs/mexico/flag-round-250.png\"\n",
    "              , \"size\": 50},\n",
    "    \n",
    "    \"['US']\": {\n",
    "      \"shape\": \"icon\",\n",
    "      \"icon\": {\n",
    "        \"face\": \"FontAwesome\",\n",
    "        \"code\": \"\\uf072\",\n",
    "        \"color\": \"#00FF00\"\n",
    "          }, \"size\": 37\n",
    "      }\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dcbb1d97",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%gremlin -p v,inv -g country\n",
    "g.V().hasLabel('airport').has('code','CZM').out('route').path().by(valueMap())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "438bc451",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "\n",
    "What we have demonstrated in this notebook is only a small set of the options and combinations available for customizing the appearance of groups within the notebook.  Please refer to the [Vis.js groups](https://visjs.github.io/vis-network/docs/network/groups.html#) documentation for a complete list of the options available."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}